package pt.unl.fct.di.apdc.firstwebapp.util;

import java.util.UUID;

public class AuthToken {
	public static final long EXPIRATION_TIME = 1000*60*60*2; //2h
	public String username;
	public String tokenID;
	public long creationData;
	public long expirationData;
	public String otherUserRequest;

	public AuthToken(){}
	
	public AuthToken(String tokenID, String username, String otherUserRequest) {
		this.username = username;
		this.tokenID = tokenID;
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
		this.otherUserRequest = otherUserRequest;
	}
	
	public AuthToken(String tokenID, String username) {
		this.username = username;
		this.tokenID = tokenID;
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
		this.otherUserRequest = "";
	}
	
	public AuthToken(String username, boolean checked) {
		this.username = username;
		this.tokenID = UUID.randomUUID().toString();
		this.creationData = System.currentTimeMillis();
		int multiplier = 1;
		if (checked){
			multiplier = 10;
		}
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME*multiplier;
		this.otherUserRequest = "";
	}
}