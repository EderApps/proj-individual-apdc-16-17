package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;
import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;


@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();
	
	public LoginResource() { } //Nothing to be done here...


	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data, 
			                  @Context HttpServletRequest request,
			                  @Context HttpHeaders headers) {
		System.out.println("TESTE");

		LOG.fine("Attempt to login user: " + data.username);
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			
			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct
				
				// Construct the logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_login_ip", request.getRemoteAddr());
				log.setProperty("user_login_host", request.getRemoteHost());
				log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_login_time", new Date());
				// Get the user statistics and updates it
				ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
				ustats.setProperty("user_stats_failed", 0L );
				ustats.setProperty("user_stats_last", new Date());		
				
				// Batch operation
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);
				txn.commit();
				Transaction txn1 = datastore.beginTransaction();

				AuthToken token = new AuthToken(data.username, data.checked);

				Entity auth = new Entity("Token", token.tokenID);
				
				auth.setProperty("auth_tokenID", token.tokenID);
				auth.setProperty("auth_username", token.username);
				auth.setProperty("auth_expirData", token.expirationData);
				auth.setProperty("auth_creationData", token.creationData);
				
				datastore.put(txn1,auth);
				
				txn1.commit();
				// Return token
				
				
				LOG.info("User '" + data.username + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();				
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn,ustats);				
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
		
	}
	
	
	@POST
	@Path("/logingoogle")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginGoogle(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data, 
			                  @Context HttpServletRequest request,
			                  @Context HttpHeaders headers) {
		System.out.println("TESTE");

		LOG.fine("Attempt to login user: " + data.username);
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			
			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct
				
				// Construct the logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_login_ip", request.getRemoteAddr());
				log.setProperty("user_login_host", request.getRemoteHost());
				log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_login_time", new Date());
				// Get the user statistics and updates it
				ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
				ustats.setProperty("user_stats_failed", 0L );
				ustats.setProperty("user_stats_last", new Date());		
				
				// Batch operation
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);
				txn.commit();
				Transaction txn1 = datastore.beginTransaction();

				AuthToken token = new AuthToken(data.username, data.checked);

				Entity auth = new Entity("Token", token.tokenID);
				
				auth.setProperty("auth_tokenID", token.tokenID);
				auth.setProperty("auth_username", token.username);
				auth.setProperty("auth_expirData", token.expirationData);
				auth.setProperty("auth_creationData", token.creationData);
				
				datastore.put(txn1,auth);
				
				txn1.commit();
				// Return token
				
				
				LOG.info("User '" + data.username + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();				
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn,ustats);				
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			Entity user = new Entity("User", data.username);
			user.setProperty("user_email", data.username);

			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));

			user.setUnindexedProperty("user_creation_time", new Date());
			datastore.put(txn,user);
			LOG.info("User registered " + data.username);

			txn.commit();
			
			Transaction txn1 = datastore.beginTransaction();

			AuthToken token = new AuthToken(data.username, data.checked);

			Entity auth = new Entity("Token", token.tokenID);
			
			auth.setProperty("auth_tokenID", token.tokenID);
			auth.setProperty("auth_username", token.username);
			auth.setProperty("auth_expirData", token.expirationData);
			auth.setProperty("auth_creationData", token.creationData);
			
			datastore.put(txn1,auth);
			
			txn1.commit();
			return Response.ok(g.toJson(token)).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
		
	}
	
	@DELETE
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogout(pt.unl.fct.di.apdc.firstwebapp.util.AuthToken token) {
		System.out.println("tokenid "+token.tokenID);
		System.out.println("Attempt to logout user: " + token.username);

		LOG.fine("Attempt to logout user: " + token.username);
		Transaction txn = datastore.beginTransaction();
		try {
				Key tokenKey = KeyFactory.createKey("Token", token.tokenID);
			
				//Entity tokenEnt = datastore.get(tokenKey);
			
				datastore.delete(txn, tokenKey);
				txn.commit();
				return Response.ok("{}").build();		
							
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
		
	}
	
	@POST
	@Path("/registerOccurrence")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAddress(pt.unl.fct.di.apdc.firstwebapp.util.OccurrenceData occurrence) {
		System.out.println("Attempt to register Occurrence : " + occurrence.description);

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("Occurrence", occurrence.description);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("Occurrence", occurrence.description);
			user.setProperty("occurrence_description", occurrence.description);
			user.setProperty("occurrence_location", occurrence.location);
			user.setProperty("occurrence_id", occurrence.occID);
			user.setProperty("occurrence_type",occurrence.typeOc);
			user.setProperty("occurrence_whodid",occurrence.whodid);
			
			user.setUnindexedProperty("occurrence_creation_time", new Date());
			datastore.put(txn,user);
			LOG.info("Occurrence registered " + occurrence.description);

			txn.commit();
			
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
		
	}
	
	@GET
	@Path("/getAllOccurrences")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrences() {

			Query ctrQuery = new Query("Occurrence");
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			System.out.println("RESULTS:::  "+ results);
			return Response.ok(g.toJson(results)).build();		
							
		
		
	}
	
	@DELETE
	@Path("/checkTimes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkTimes(){
		
		Transaction txn = null;
		Query ctrQuery = new Query("Token");
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		for(int i = 0; i < results.size();i++){
			if ((long)(results.get(i).getProperty("auth_expirData")) <= System.currentTimeMillis() ){
				txn = datastore.beginTransaction();
				Key key = KeyFactory.createKey("Token", (String)(results.get(i).getProperty("auth_tokenID")));
				datastore.delete(txn, key);
				txn.commit();
			}
		}
		System.out.println(results.size());
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUsernameAvailable( pt.unl.fct.di.apdc.firstwebapp.util.LoginData data ) {
		
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date yesterday = cal.getTime();
				
				// Obtain the user login statistics
				Filter propertyFilter =
					    new FilterPredicate("user_login_time", FilterOperator.GREATER_THAN_OR_EQUAL, yesterday);
				Query ctrQuery = new Query("UserLog").setAncestor(KeyFactory.createKey("User", data.username))
						             .setFilter(propertyFilter)
						             .addSort("user_login_time", SortDirection.DESCENDING);
				ctrQuery.addProjection(new PropertyProjection("user_login_time", Date.class));
				ctrQuery.addProjection(new PropertyProjection("user_login_ip", String.class));
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(30));
				
/*				List<Date> loginDates = new ArrayList();
				for(Entity userlog:results) {
						loginDates.add((Date) userlog.getProperty("user_login_time"));
				}
*/				return Response.ok(g.toJson(results)).build();
			
			} else {
				LOG.warning("Wrong password for username: " + data.username );
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
	/*@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getLoggedUser(@PathParam("id") String id) {
		Key userKey = KeyFactory.createKey("User", id);
		AuthToken auth = new AuthToken();
		System.out.println("EPAAA" + id);
		try {
			Entity user = datastore.get(userKey);
			return Response.ok(g.toJson(user)).build();
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			return Response.status(Status.FORBIDDEN).build();
		}		
	}*/
	
	
}

