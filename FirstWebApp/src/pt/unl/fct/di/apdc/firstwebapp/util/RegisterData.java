package pt.unl.fct.di.apdc.firstwebapp.util;


public class RegisterData {

	public String username;
	public String name;
	public String email;
	public String homePhone;
	public String personalPhone;
	public String street;
	public String region;
	public String zipCode;
	public String NIF;
	public String CC;
	public String password;
	public String confirmation;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String username, String name, String email,String homePhone,
			String personalPhone,String street,String region,String zipCode,String NIF,String CC,
			String password, String confirmation) {
		
			this.username = username;
			this.name = name;
			this.email = email;
			this.homePhone = homePhone;
			this.personalPhone = personalPhone;
			this.street = street;
			this.region = region;
			this.zipCode = zipCode;
			this.NIF = NIF;
			this.CC = CC;
			this.password = password;
			this.confirmation = confirmation;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return validField(username) &&
			   validField(name) &&
			   validField(email) &&
			   validField(password) &&
			   validField(confirmation) && validField(street) &&
			   validField(zipCode) &&
			   validField(region) &&
			   validField(NIF) &&
			   validField(CC) && validField(homePhone) &&
			   validField(personalPhone) &&
			   password.equals(confirmation) &&
			   email.contains("@");		
	}
	
}