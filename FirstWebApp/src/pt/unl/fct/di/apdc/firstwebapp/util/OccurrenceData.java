package pt.unl.fct.di.apdc.firstwebapp.util;

import java.util.UUID;

public class OccurrenceData {
	public String typeOc;
	public String location;
	public String description;
	public String whodid;
	public String occID;
	
	public OccurrenceData(){}
	
	public OccurrenceData(String typeOc, String location, String description) {
		this.typeOc = typeOc;
		this.location = location;
		this.description = description;
		this.whodid = "";
		this.occID = UUID.randomUUID().toString();

	}
	
	public OccurrenceData(String typeOc, String location, String description, String whodid) {
		this.typeOc = typeOc;
		this.location = location;
		this.description = description;
		this.whodid = whodid;
		this.occID = UUID.randomUUID().toString();

	}
}
