
window.onload = function() {
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrences/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			
			
			for (i = 0; i < response.length; i++) { 
				
				var address = response[i].propertyMap.occurrence_location;
				var type = response[i].propertyMap.occurrence_type;
				var description = response[i].propertyMap.occurrence_description;
				var datei = response[i].propertyMap.occurrence_creation_time.value;
				console.log(datei);
			    document.getElementById('allOccs').innerHTML += '<li>Location: ' + address + ' <br>Type: '+ type +' <br>Description: '+ description +' <br>Date Issued: '+datei+'</li> <br>';

				//showMapOne(address);
			}

		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}