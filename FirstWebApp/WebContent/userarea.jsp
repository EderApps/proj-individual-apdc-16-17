<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>User Area</title>
          <link rel="stylesheet" href="css/restfrontend.css">

  </head>

  <body>
      <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.jsonify-0.3.1.min.js"></script>
  <script type="text/javascript" src="js/userarea.js"></script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNp1n4qTqKUyxRgEZz8emtAVx14ciycMY&libraries=places&callback=showMapInit"></script>
      <script src="https://apis.google.com/js/client:platform.js?onload=onLoad" async defer></script>
      <meta name="google-signin-client_id" content="247126777527-r88mm3n66uogplmvbehtdenci5tgvdof.apps.googleusercontent.com">

    <h1>Welcome <span id="user"> user </span>!</h1>
	<p>This is your profile!</p>
      <p>Token ID: <span id="tokenid"> token </span></p>
      <p>Creation Data: <span id="expiration"> expir </span></p>
      <p>Expiration Data: <span id="creation"> creat </span></p>
    <table>
      <button id = "logout" class="buttonlogout">Logout</button>
              <p></p>
      <button id = "getAllMaps" class="buttonlogout">Get All Occurrences</button>

        <p> </p>
        <p>Register an occurence</p>
        <form name="getMap" class="pure-form pure-form-stacked" accept-charset="utf-8">
    <fieldset>
        Type of Occurence 
        <br>
        <select name="typeOc" id="typeOc">
                <option value="Light">Lights</option>
                <option value="Street">Streets</option>
                <option value="Seasonal">Seasonal</option>
                <option value="Trash">Trash</option>
                <option value="Vehicle">Vehicles/Parking</option>
                <option value="Graffiti">Graffitis</option>
                <option value="Tree">Trees</option>
                <option value="Other">Other</option>
        </select>
                        <p> </p>

        <label for="location">Location</label>
                        <p> </p>

      <input type="text" id="location" name="location" class="location" required>
        
    <div id="map_canvas"></div>
                <p> </p>

        <label for="description">Description</label>
                        <p> </p>

      <textarea type="text1" rows="6" cols="25"  id="description" name="description" class="description" required></textarea>
                                <p> </p>

        <label for="whodid">Who did it? (optional)</label>
                        <p> </p>

        <textarea type="text1" rows="6" cols="25"  id="whodid" name="whodid" class="whodid"></textarea>
                        <p> </p>

        <input type="submit" class="getMap" value="Register occurence">

    </fieldset>
        </form>
    </table>
      
  </body>
</html>

