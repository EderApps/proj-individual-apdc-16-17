<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Occurrences</title>
          <link rel="stylesheet" href="css/restfrontend.css">

  </head>

  <body>
      <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.jsonify-0.3.1.min.js"></script>
  <script type="text/javascript" src="js/occurrences.js"></script>
    <h1>All occurrences</h1>
	
     <ul id="allOccs">
      </ul>
      
  </body>
</html>

