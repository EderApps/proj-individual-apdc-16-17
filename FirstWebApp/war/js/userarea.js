

logout = function(event) {
	var token = localStorage.getItem('tokenID');
	var tokenname = localStorage.getItem('tokenIDname');
	var tokenexp = localStorage.getItem('tokenIDexpir');
	var tokencre = localStorage.getItem('tokenIDcreate');
	var data = {};
	data.tokenID = token;
	data.username = tokenname;
	data.expirationData = tokenexp;
	data.creationData = tokencre;
	console.log(data);

	console.log(JSON.stringify(data));
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/logout/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
			alert("User logged out!");
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
	signOut();

}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}

function onLoad() {
    gapi.load('auth2', function() {
      gapi.auth2.init();
    });
}

var place = "";
window.onload = function() {
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/checkTimes/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			//localStorage.setItem('tokenIDname', "");
			//localStorage.setItem('tokenID', "");
			//localStorage.setItem('tokenIDcreate', "");
			//localStorage.setItem('tokenIDexpir', "");
			
			if (localStorage.getItem('tokenID') == null){
				localStorage.setItem('tokenID', "");
			}
			if (localStorage.getItem('tokenID').localeCompare("") == 0){
				window.location.href = "/";

			}
			
			
			var d = new Date();
			var t = d.getTime();
			
			if (t > localStorage.getItem('tokenIDexpir')){
				alert("Your session timed out! Please log back in.");
				signOut();

				localStorage.setItem('tokenIDname', "");
				localStorage.setItem('tokenID', "");
				localStorage.setItem('tokenIDcreate', "");
				localStorage.setItem('tokenIDexpir', "");
				window.location.href = "/";
			}
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
	
	if (localStorage.getItem('tokenID') == null){
		localStorage.setItem('tokenID', "");
	}
	if (localStorage.getItem('tokenID').localeCompare("") == 0){
		window.location.href = "/";

	}
	

	document.getElementById("user").textContent = localStorage.getItem('tokenIDname');
	document.getElementById("tokenid").textContent = localStorage.getItem('tokenID');
	document.getElementById("expiration").textContent = localStorage.getItem('tokenIDcreate');
	document.getElementById("creation").textContent = localStorage.getItem('tokenIDexpir');

	document.getElementById("logout").onclick = logout;
	document.getElementById("getAllMaps").onclick = getAllMaps;

	var frms = $('form[name="getMap"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = getAddress;
	
	 var autocomplete = new google.maps.places.Autocomplete((document.getElementById('location')));
       //google.maps.event.addListener(autocomplete, 'place_changed', function() {
       //});
	    autocomplete.addListener('place_changed', function(){
		 	var place = autocomplete.getPlace();
		 	var lat = place.geometry.location.lat();
		 	var lng = place.geometry.location.lng();

		 	map = new google.maps.Map(document.getElementById('map_canvas'), {
		 		center: {lat: lat, lng: lng},
		 		zoom: 10
		 		});
		 	
		 	var pos = new google.maps.LatLng(lat, lng);
		 	var marker = new google.maps.Marker({
		 	position: pos,
		 	map: map
		 	});
	    		
	    });

     
  }



getAllMaps = function(event){
	event.preventDefault();

	/*$.ajax({ 
		type: "POST",
		//dataType: "json",
		url: "/rest/login/getAllAddresses/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			
			
			for (i = 0; i < response.length; i++) { 
				
				var address = response[i].propertyMap.user_street;
				console.log(address);
				showMapOne(address);
			}

		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});*/
	
	window.location.href = "/occurrences";
}




getAddress = function(event){
	event.preventDefault();
	
	var data = $('form[name="getMap"]').jsonify();

	console.log(data);

	console.log(JSON.stringify(data));

	$.ajax({ 
		type: "POST",
		//dataType: "json",
		url: "/rest/login/registerOccurrence/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			//alert("User logged out!");
			alert("Occurrence Registered!");
			window.location.href = "/userarea";

		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
}

var geocoder;
var map;
//var address ="";
function showMap(address) {
	console.log("address ");

	console.log(address);
	if(!(address in window)){

	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(-34.397, 150.644);
	var myOptions = {
			zoom: 2,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	if (geocoder) {
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow(
							{ content: '<b>'+address+'</b>',
								size: new google.maps.Size(150,50)
							});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: map, 
						title:address,
					    label: {text: address, color: "black"}

					}); 
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					});

				} else {
				}
			} else {

			}
		});
	}
	}
}

function showMapOne(address) {
	if(!(address in window)){
	geocoder = new google.maps.Geocoder();
	if (geocoder) {
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow(
							{ content: '<b>'+address+'</b>',
								size: new google.maps.Size(150,50)
							});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: map, 
						title:address,
					    label: {text: address, color: "black"}

					}); 
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					});

				} else {
				}
			} else {
			}
		});
	}
	}
}

function showMapInit() {
	
	var latlng = new google.maps.LatLng(38.736946, -9.142685);
	var myOptions = {
			zoom: 2,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	
}
