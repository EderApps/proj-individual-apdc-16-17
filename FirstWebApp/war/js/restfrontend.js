captureData = function(event) {
	var data = $('form[name="login"]').jsonify();
	
    var checked = document.getElementById("check").checked;
    data.checked = checked;
    console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			// Store token id for later use in localStorage
			console.log(response.username);
			console.log(response.tokenID);
			console.log(response.creationData);
			console.log(response.expirationData);

			localStorage.setItem('tokenIDname', response.username);
			localStorage.setItem('tokenID', response.tokenID);
			localStorage.setItem('tokenIDcreate', response.creationData);
			localStorage.setItem('tokenIDexpir', response.expirationData);

			window.location.href = "/userarea";

		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function onFailure(error) {
    alert(error);
}
function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 380,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        $('.userContent').html('');
        $('#gSignIn').slideDown('slow');
    });
}

function onSuccess(googleUser) {

	  var profile = googleUser.getBasicProfile();
	  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	  console.log('Name: ' + profile.getName());
	  console.log('Image URL: ' + profile.getImageUrl());
	  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	    var checked = document.getElementById("check").checked;
	  var data = {};
	  data.username = profile.getEmail();
	  data.password = profile.getId();
	  data.checked = checked;
	  console.log(JSON.stringify(data))
	  $.ajax({
			type: "POST",
			url: "/rest/login/logingoogle",
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				// Store token id for later use in localStorage
				console.log(response.username);
				console.log(response.tokenID);
				console.log(response.creationData);
				console.log(response.expirationData);

				localStorage.setItem('tokenIDname', response.username);
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenIDcreate', response.creationData);
				localStorage.setItem('tokenIDexpir', response.expirationData);

				window.location.href = "/userarea";

			},
			error: function(response) {
				//alert("Error: "+ response.status);
			},
			data: JSON.stringify(data)
		});
}

captureDataReg = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("User registered! Please login.");
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	if (localStorage.getItem('tokenID') == null){
		localStorage.setItem('tokenID', "");
	}
	if (localStorage.getItem('tokenID').localeCompare("") != 0){
		window.location.href = "/userarea";

	}


	var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = captureData;

	var frms1 = $('form[name="register"]');     //var frms = document.getElementsByName("login");
	frms1[0].onsubmit = captureDataReg;
	
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/checkTimes/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			
			
			//alert("Checked times!");
			
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}