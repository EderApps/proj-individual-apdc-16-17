<doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Almada Connect</title>
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/pure/0.6.2/pure-min.css">
  <link rel="stylesheet" href="css/login.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.jsonify-0.3.1.min.js"></script>
  <script type="text/javascript" src="js/restfrontend.js"></script>
<script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
<meta name="google-signin-client_id" content="247126777527-r88mm3n66uogplmvbehtdenci5tgvdof.apps.googleusercontent.com">

  <div class="login-wrap">
        
    <div class="login-html">

        <img class="logo"  src="https://storage.googleapis.com/fit-sanctum-159416.appspot.com/aclogo.png">
        <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
        <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
        <div class="login-form">
            <div class="sign-in-htm">
                  <form name="login" class="pure-form pure-form-stacked" accept-charset="utf-8">
                    <fieldset>

                <div class="group">
                    <label for="username" class="label">Username</label>
                    <input type="text" class="input" id="username" name="username" placeholder="username" required>
                </div>
                <div class="group">
                    <label for="password" class="label">Password</label>
                    <input type="password" class="input" id="password" name="password" placeholder="password" required>
                </div>
                <div class="group">
                    <input id="check" type="checkbox" class="check" checked>
                    <label for="check"><span class="icon"></span> Keep me Signed in</label>
                </div>
                <div class="group">
                    <input type="submit" class="button" value="Sign In">
                </div>
                        <!-- HTML for render Google Sign-In button -->
<div id="gSignIn"></div>
<!-- HTML for displaying user details -->
<div class="userContent"></div>
                <div class="hr"></div>
                
                      </fieldset>
                </form>
            </div>
            <div class="sign-up-htm">
                <form name="register" class="pure-form pure-form-stacked" accept-charset="utf-8">
                    <fieldset>
                <div class="group">
                    <label for="username" class="label">Username</label>
                    <input type="text" class="input" id="username" name="username" placeholder="username" required>
                </div>
                <div class="group">
                    <label for="name" class="label">Full name</label>
                    <input type="text" class="input" id="name" name="name" placeholder="name" required>
                </div>
                <div class="group">
                    <label for="email" class="label">Email Address</label>
                    <input id="email" name ="email" type="text" class="input" placeholder="email" required>
                </div>
                <div class="group">
                    <label for="homePhone" class="label">Home Phone</label>
                    <input id="homePhone" name ="homePhone" type="text" class="input" placeholder="home phone" required>
                </div>
                <div class="group">
                    <label for="personalPhone" class="label">Personal Phone</label>
                    <input id="personalPhone" name ="personalPhone" type="text" placeholder="personal phone" class="input" required>
                </div>
                <div class="group">
                    <label for="street" class="label">Street</label>
                    <input id="street" name ="street" placeholder="street" type="text" class="input" required>
                </div>
                <div class="group">
                    <label for="region" class="label">Region</label>
                    <input id="region" placeholder="region" name ="region" type="text" class="input" required>
                </div>
                <div class="group">
                    <label for="zipCode" class="label">Zip Code</label>
                    <input id="zipCode" placeholder="zip code" name ="zipCode" type="text" class="input" required>
                </div>
                <div class="group">
                    <label for="CC" class="label">CC</label>
                    <input id="CC" placeholder="CC" name ="CC" type="text" class="input" required>
                </div>
                <div class="group">
                    <label for="NIF" class="label">NIF</label>
                    <input id="NIF" placeholder="NIF" name ="NIF" type="text" class="input" required>
                </div>
                
                <div class="group">
                    <label for="password" class="label">Password</label>
                    <input id="password" placeholder="password" name ="password" type="password" class="input" data-type="password" required>
                </div>
                <div class="group">
                    <label for="confirmation" class="label">Confirm Password</label>
                    <input id="confirmation" placeholder="confirm" name ="confirmation" type="password" class="input" data-type="password" required>
                </div>
                
                <div class="group">
                    <input type="submit" class="button" value="Sign Up">
                </div>
                <div class="hr"></div>
                <div class="foot-lnk">
                    <label for="tab-1">Already Member?</a>
                </div>
                        </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>

</html>
